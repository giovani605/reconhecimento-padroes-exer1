
# coding: utf-8

# In[3]:

from skimage import io, color, exposure

from sklearn.cluster import KMeans
import numpy as np
import os
from matplotlib import pyplot as plt
#get_ipython().magic('pylab inline')

d0 = []
d1 = []
d2 = []

matrizFinal = []
for img in os.listdir('flores/'):
    im=io.imread('flores//' + img)          
    print('Processing ' + img)
    
    im = color.rgb2hsv(im);  
   
    #descritor simples - soma de histograma
    aux1 = [x/64 for x in im[:,:,0]]
    aux2 = [x/16 for x in im[:,:,1]]
    aux3 = [x/4 for x in im[:,:,2]]
    idx = np.add(aux1,np.add(aux2,aux3))
    d0.append(np.sum(idx))
    
    valorHistograma = np.sum(idx)


    #média do valor
    idx1 = np.mean(im[:,:,2])
    d1.append(idx1)    

    mediaValor = idx1
    
    #média de um ajuste do valor
    aux = [np.square(x*2) for x in im[:,:,2]]
    idx2 = np.mean(aux);
    d2.append(idx2)

    mediaAjuste = idx2
    
    # vetor com os 3 valores que vou usar no treinamento unsupervisionado
    matrizFinal.append(np.array([valorHistograma,mediaValor,mediaAjuste]))
    
random_state = 170
matriz = np.array(matrizFinal)


print(matriz.shape)
print(matriz)

clf = KMeans(4,random_state=random_state).fit(matriz)
print(clf)
predicao = clf.predict(matriz)
print(predicao)
print(predicao.reshape(len(predicao),1))

## plotando
plt.figure()
M = np.hstack((matriz,predicao.reshape(len(predicao),1)))
print(M.shape)

for x in M:
    print(x)
    if(x[3] == 0):
        plt.plot(x[0], x[1],'k*')
    if(x[3] == 1):
        plt.plot(x[0], x[1],'rs')
    if(x[3] == 2):
        plt.plot(x[0], x[1],'yo')
    if(x[3] == 3):
        plt.plot(x[0], x[1],'bo')

    



plt.show()
'''
#plots dos resultados
plt.figure()
#Separa cada flor na mao
plt.plot(d1[0:5], d0[0:5], 'rs')
plt.plot(d1[5:10], d0[5:10], 'k*')
plt.plot(d1[11:15], d0[11:15], 'yo')
plt.plot(d1[16:20], d0[16:20], 'bo')

plt.figure()
plt.plot(d2[0:5], d0[0:5], 'rs')
plt.plot(d2[5:10], d0[5:10], 'k*')
plt.plot(d2[11:15], d0[11:15], 'yo')
plt.plot(d2[16:20], d0[16:20], 'bo')
'''

# Tenho 3 features d0, d1 ,d2




# In[ ]:



